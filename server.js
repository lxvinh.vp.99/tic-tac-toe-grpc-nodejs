let grpc = require("grpc");
var protoLoader = require("@grpc/proto-loader");

const server = new grpc.Server();
const SERVER_ADDRESS = "0.0.0.0:5001";

// Load protobuf
let proto = grpc.loadPackageDefinition(
  protoLoader.loadSync("protos/chat.proto", {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
  })
);

let users = [];
let db = [['*', '*', '*'], ['*', '*', '*'], ['*', '*', '*']];
let numberOfPlayer = 0;
let lastTurn = null;
let numberOfTurn = 0;
// Receive message from client joining
function join(call, callback) {
  if (numberOfPlayer >= 2) {
    notifyChat({ user: "Server", text: "full" }, [call]);
    return;
  }
  users.push(call);
  numberOfPlayer++;
  if (numberOfPlayer === 1) {
    notifyChat({ user: "Server", text: "X-player" }, [users[0]]);
  } else if (numberOfPlayer === 2) {
    notifyChat({ user: "Server", text: "O-player" }, [users[1]]);
    notifyChat({ user: "Server", text: "start game ..." }, users);
  }
}

// Receive message from client
function send(call, callback) {
  const message = call.request;
  if (numberOfPlayer < 2) {
    notifyChat({ user: "Server", text: "Please wait O-player join" }, users);
    return;
  }
  if (message.user === "X-player" && lastTurn === "X-player") {
    notifyChat({ user: "Server", text: "This turn is O-player" }, [users[0]]);
    return;
  } else if (message.user === "O-player" && (!lastTurn || lastTurn === "O-player")) {
    notifyChat({ user: "Server", text: "This turn is X-player" }, [users[1]]);
    return;
  }
  const arr = message.text.split('-');
  if (checkMarkIsValid(arr)) {
    db[arr[0]][arr[1]] = message.user.slice(0, 1);
    lastTurn = message.user;
    numberOfTurn++;
    logXO();
    notifyChat(message, users);
    const typeWin = checkResult();
    if (typeWin) {
      notifyChat({ user: "Server", text: `${typeWin}-player win!!!` }, users);
      return;
    }
    if (checkMarkFull()) {
      notifyChat({ user: "Server", text: `Draw!!!` }, users);
      return;
    }
  } else {
    if (message.user === "X-player") {
      notifyChat({ user: "Server", text: "Your mark is invalid or this mark is not empty!" }, [users[0]]);
    } else {
      notifyChat({ user: "Server", text: "Your mark is invalid or this mark is not empty!" }, [users[1]]);
    }
    return;
  }
}

function checkMarkIsValid(arr) {
  if (!arr || arr.length !== 2) return false;
  let check = true;
  arr.forEach(coor => {
    let x = parseInt(coor);
    if ((coor.length !== 1) || isNaN(x) || (x < 0) || (x > 2)) {
      check = false;
    };
  });
  if (check && db[arr[0]][arr[1]] !== '*') return false;
  return check;
}

// Send message to sendToUsers clients
function notifyChat(message, sendToUsers) {
  sendToUsers.forEach(user => {
    user.write(message);
  });
}

function logXO() {
  console.log(`${numberOfTurn}: `);
  for (let i = 0; i < 3; i++) {
    let str = "";
    for (let j = 0; j < 3; j++) {
      str = str + " " + db[i][j];
    };
    console.log(str);
  }
}

function checkResult() {
  let typeWin = null;
  // check row
  for (let i = 0; i < 3; i++) {
    if (db[i][0] !== "*" && (db[i][1] === db[i][0]) && (db[i][2] === db[i][0])) {
      typeWin = db[i][0];
      return typeWin;
    }
  }

  // check column
  for (let j = 0; j < 3; j++) {
    if (db[0][j] !== "*" && (db[1][j] === db[0][j]) && (db[2][j] === db[0][j])) {
      typeWin = db[0][j];
      return typeWin;
    }
  }

  // check x
  if (db[0][0] !== "*" && (db[1][1] === db[0][0]) && (db[2][2] === db[0][0])) {
    typeWin = db[0][0];
    return typeWin;
  }
  if (db[0][2] !== "*" && (db[1][1] === db[0][2]) && (db[2][0] === db[0][2])) {
    typeWin = db[0][2];
    return typeWin;
  }

  return typeWin;
}

function checkMarkFull() {
  for (let i = 0; i < 3; i++) {
    for (let j = 0; j < 3; j++) {
      if (db[i][j] === "*") {
        return false;
      }
    }
  }
  return true;
}

// Define server with the methods and start it
server.addService(proto.example.Chat.service, { join: join, send: send });

server.bind(SERVER_ADDRESS, grpc.ServerCredentials.createInsecure());

server.start();