let grpc = require("grpc");
var protoLoader = require("@grpc/proto-loader");
var readline = require("readline");

//Read terminal Lines
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

//Load the protobuf
var proto = grpc.loadPackageDefinition(
  protoLoader.loadSync("protos/chat.proto", {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true
  })
);

const REMOTE_SERVER = "0.0.0.0:5001";

let username;

//Create gRPC client
let client = new proto.example.Chat(
  REMOTE_SERVER,
  grpc.credentials.createInsecure()
);

//Start the stream between server and client
function startChat() {
  let channel = client.join({ user: username });
  channel.on("data", onData);

  rl.on("line", function (text) {
    client.send({ user: username, text: text }, res => { });
  });
}

//When server send a message
function onData(message) {
  if (message.user == username) {
    return;
  }
  if (message.user === "Server" && message.text === "X-player" && !username) {
    username = "X-player";
    console.log(`${message.user}: You are ${message.text}`);
    return;
  } else if (message.user === "Server" && message.text === "O-player" && !username) {
    username = "O-player";
    console.log(`${message.user}: You are ${message.text}`);
    return;
  }
  console.log(`${message.user}: ${message.text}`);
}

//Ask user name then start the chat
rl.question("Do you want enjoy? (Enter continue)", () => {
  startChat();
});